<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLmsCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_courses', function (Blueprint $table) {
            $table->id();
            $table->string('idcourse')->nullable();
            $table->string('shortname');
            $table->string('categorysortorder')->nullable();
            $table->string('fullname');
			$table->longText('summary')->nullable();
			$table->string('visible')->nullable();
			$table->longText('image')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('availability');
            $table->string('model');
            $table->integer('price');
            $table->string('accesscode','10');
            $table->longText('topik')->nullable();
            $table->string('durasi')->nullable();
            $table->string('level')->nullable();
            $table->string('rekomendasi')->nullable();
            $table->string('video')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_courses');
    }
}
