<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLmsEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lms_enrollments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('idcourse')->unsigned()->nullable();
            $table->bigInteger('iduser')->unsigned()->nullable();
            $table->string('enrollmenttype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lms_enrollments');
    }
}
