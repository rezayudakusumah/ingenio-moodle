<?php

use GuzzleHttp\Client;
use App\Models\LmsSettings;
use App\Models\LmsEnrollments;
use App\Models\LmsCategories;

    function get_user_enrol($idcourse, $iduser){

        $enrollment = LmsEnrollments::where('idcourse', $idcourse)->where('iduser', $iduser)->count();

        return $enrollment;
        // $settings = LmsSettings::get();

        // foreach($settings as $getsettings){}

        // $client = new Client();

        // $function = 'core_enrol_get_users_courses';

        // $getEnroll = $client->request('GET', $getsettings->siteurl.'/webservice/rest/server.php?wstoken='.$getsettings->token.'&wsfunction='.$function.'&moodlewsrestformat=json&userid='.$iduser);

        // $dataEnroll = $getEnroll->getBody();
        // $dataEnroll = json_decode($dataEnroll);
        
        // return $dataEnroll;
    }

    function get_name_category($id){
        $cat = LmsCategories::where('idcategory', $id)->first();
        
        if(empty($cat)){
            return false;
        }else{
            return $cat->name;
        }
    }