<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LmsCategories extends Model
{
    use HasFactory;

    protected $table = 'lms_categories';

    public function courses(){
        return $this->hasMany('\App\Models\LmsCourses', 'idcategory', 'idcategory');
    }
}
