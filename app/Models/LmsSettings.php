<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LmsSettings extends Model
{
    use HasFactory;

    protected $table = 'lms_settings';
}
