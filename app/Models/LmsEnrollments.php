<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LmsEnrollments extends Model
{
    use HasFactory;

    protected $table = 'lms_enrollments';

    public function course()
    {
        return $this->hasOne('App\Models\LmsCourses','idcourse','idcourse');
    }
}
