<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LmsCourses extends Model
{
    use HasFactory;

    protected $table = 'lms_courses';

    public function enrollments(){
        return $this->hasMany('\App\Models\LmsEnrollments', 'idcourse', 'idcourse');
    }

    public function category(){
        return $this->hasOne('\App\Models\LmsCategories', 'idcategory', 'idcategory');
    }
}
