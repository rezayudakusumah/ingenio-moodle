<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LmsSettings;
use App\Models\LmsEnrollments;
use GuzzleHttp\Client;
use Auth;

class EnrolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    public function enrol($id){
        $iduser = Auth::user()->idmoodle;
        
        $api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$function = 'enrol_manual_enrol_users';
		$client = new Client();
		
		$postEnroll = $client->request('POST', $getapi->siteurl.'/webservice/rest/server.php?wstoken='.$getapi->token.'&wsfunction='.$function.'&moodlewsrestformat=json',
    
        array(
            'form_params' => array(
                'enrolments' => array(
                    array(
                        'roleid' => '5',
                        'userid' => $iduser,
                        'courseid' => $id,
                    )
                )
            )
        )

    );

		$dataEnroll = $postEnroll->getBody();
		$dataEnroll = json_decode($dataEnroll);
		
        // return dd($dataEnroll);

        if(isset($dataEnroll->message)){
			return redirect()->back()->with('message', $dataEnroll->message);
		}else{
			$cek = LmsEnrollments::where('idcourse', $id)->where('iduser', '4')->first();
			
			if(empty($cek)){
				$enroll = new LmsEnrollments;
				$enroll->idcourse = $id;
				$enroll->iduser = $iduser;
				$enroll->save();
			}
			return redirect('/my')->with('message', 'Selamat, Anda telah terenroll pada kelas Silahkan klik Open Course untuk melanjutkan pembelajaran.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
