<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LmsSettings;
use GuzzleHttp\Client;
use App\Models\LmsCourses;
use App\Models\LmsEnrollments;
use Auth;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $settings = LmsSettings::get();
		// foreach($settings as $getsettings){}

		// $client = new Client();

		// $functioncourse = 'core_course_get_courses_by_field';

		// $getCourse = $client->request('GET', $getsettings->siteurl.'/webservice/rest/server.php?wstoken='.$getsettings->token.'&wsfunction='.$functioncourse.'&moodlewsrestformat=json');

		// $dataCourse = $getCourse->getBody();
		// $dataCourse = json_decode($dataCourse);
        
        // $data = [
        //     'courses' => $dataCourse->courses
        // ];

        $courses = LmsCourses::orderBy('fullname')->get();

        $data = [
             'courses' => $courses
        ];

        // return $data;
		return view('courses.catalog', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = LmsCourses::where('idcourse', $id)->first();
        
        $data = [
            'course' => $course
        ];
        
        return view('courses.catalog-detail', $data);
    }

    public function my(){
        $iduser = Auth::user()->idmoodle;
        
        $courses = LmsEnrollments::where('iduser', $iduser)->with('course')->get();
        $data = [
            'courses' => $courses
        ];
        // return $data;
        return view('courses.my', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
