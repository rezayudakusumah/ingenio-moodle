<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LmsCategories;
use GuzzleHttp\Client;
use App\Models\LmsSettings;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
			'category' => LmsCategories::get(),
		];
		
		return view('admin.categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
			'category' => LmsCategories::get(),
		];

        return view('admin.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
			'parent' => 'required',
			'name' => 'required',
		]);
		
		$api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$function = 'core_course_create_categories';
		$client = new Client();
		$postCategory = $client->request('POST', $getapi->siteurl . '/webservice/rest/server.php?wstoken=' . $getapi->token . '&wsfunction='.$function.'&moodlewsrestformat=json',		
			array(
				'form_params' => array(
					'categories' => array(
						array(
							'name' => $request->name,
							'parent' => $request->parent,
							'description' => $request->description,
						)
					)
				)
			)
		);
		
		$dataCat = $postCategory->getBody();
		$dataCat = json_decode($dataCat);
		
		if(isset($dataCat->message)){
			return redirect()->back()->with('message', $dataCat->message);
            // return dd($dataCat);
		}else{
			$category = new LmsCategories;
			$category->idcategory = $dataCat[0]->id;
			$category->parent = $request->parent;
			$category->name = $request->name;
			$category->description = $request->description;
			$category->visible ='1';
			$category->coursecount ='0';
			$category->save();
			
			return redirect('/admin/categories');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
