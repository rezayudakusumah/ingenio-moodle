<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use GuzzleHttp\Client;
use App\Models\LmsSettings;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
			'user' => User::orderBy('id','DESC')->get(),
		];
		
		return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $validateData = $request->validate([
			'fullname' => 'required',
			'email' => 'required',
			'password' => 'required',
		]);
		
		$name = explode(" ",$request->fullname);
		$countname = count($name);
		
		if($countname == 1){
			$firstname = $name[0];
			$lastname = "lms";
		}else{
			$firstname = $name[0];
			$lastname = $name[1];
		}
		
		$email = explode("@",$request->email);
		
		$countID = User::max('id');
		
		$api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$functionuser = 'core_user_create_users';

		$client = new Client();
		$postUser = $client->request('POST', $getapi->siteurl . '/webservice/rest/server.php' . '?wstoken=' . $getapi->token . '&wsfunction='.$functionuser.'&moodlewsrestformat=json',
			array(
				'form_params' => array(
					'users' => array(
						array(
							'username' => $request->username,
							'firstname' => $firstname,
							'lastname' => $lastname,
							'email' => $request->email,
							// 'password' => $request->password,
							'password' => '1rhMJZ7k7CRoGdzSz0eN1rhMJZ7k7C^RoGdzSz0eN',
							'lang' => 'en'
						)
					)
				)
			)
		);

		$dataUser = $postUser->getBody();
		$dataUser = json_decode($dataUser);
		
		if(isset($dataUser->message)){
			return redirect()->back()->with('message', $dataUser->message);
		}else{
			$Users = new User;
			$Users->name = $request->fullname;
			$Users->email = $request->email;
			$Users->password = bcrypt($request->password);
			$Users->bpwd = $request->password;
			$Users->username = $request->username;
			$Users->level = $request->level;
			$Users->accounttype = 'manual';
			$Users->email_verified_at = date("Y-m-d h:i:s");
			$Users->idmoodle = $dataUser[0]->id;
			$Users->save();
			
			if($Users != null){
				return redirect('/admin/user')->with('success', 'Add New Data User Success');
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {	
        $data = [
			'users' => User::where('idmoodle', $id)->first(),
		];

		return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
			'fullname' => 'required',
			'email' => 'required',
			'password' => 'required',
		]);
		
		$name = explode(" ",$request->fullname);
		$countname = count($name);
		
		if($countname == 1){
			$firstname = $name[0];
			$lastname = "lms";
		}else{
			$firstname = $name[0];
			$lastname = $name[1];
		}
		
		$email = explode("@",$request->email);
		
		$countID = User::max('id');
		
		$api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$Users = User::where('idmoodle', $id)->first();
		
		$function = 'core_user_update_users';
		$client = new Client();
		
		if($Users->accounttype == 'import'){
			$getUpdate= $client->request('POST', $getapi->siteurl.'/webservice/rest/server.php?wstoken='.$getapi->token.'&wsfunction='.$function.'&moodlewsrestformat=json',
				array(
					'form_params' => array(
						'users' => array(
							array(
								'id' => $id,
								'username' => $email[0].$countID,
								'firstname' => $firstname,
								'lastname' => $lastname,
								'email' => $request->email,
								'password' => $request->password,
								// 'password' => '1rhMJZ7k7CRoGdzSz0eN1rhMJZ7k7C^RoGdzSz0eN',
								'lang' => 'en'
							)
						)
					)
				)
			);
		}else{
			$getUpdate= $client->request('POST', $getapi->siteurl.'/webservice/rest/server.php?wstoken='.$getapi->token.'&wsfunction='.$function.'&moodlewsrestformat=json',
				array(
					'form_params' => array(
						'users' => array(
							array(
								'id' => $id,
								'username' => $email[0].$countID,
								'firstname' => $firstname,
								'lastname' => $lastname,
								'email' => $request->email,
								// 'password' => $request->password,
								'password' => '1rhMJZ7k7CRoGdzSz0eN1rhMJZ7k7C^RoGdzSz0eN',
								'lang' => 'en'
							)
						)
					)
				)
			);
			
		}

		$getUpdate = $getUpdate->getBody();
		$getUpdate = json_decode($getUpdate);

		if(isset($getUpdate->message)){
			return redirect()->back()->with('message', $getUpdate->message);
		}else{
			$Users->username = $request->username;
			$Users->name = $request->fullname;
			$Users->email = $request->email;
			$Users->password = bcrypt($request->password);
			$Users->save();
			
			return redirect('admin/user')->with('success', 'Update data users success');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$function = 'core_user_delete_users';
		$client = new Client();
		
		$delUser= $client->request('POST', $getapi->siteurl.'/webservice/rest/server.php?wstoken='.$getapi->token.'&wsfunction='.$function.'&moodlewsrestformat=json',
			array(
				'form_params' => array(
					'userids' => array($id),
				)
			)
		);

		$dataDel = $delUser->getBody();
		$dataDel = json_decode($dataDel);
		
		if(isset($dataDel->message)){
			return redirect()->back()->with('message', $dataDel->message);
		}else{
			$Users = User::where('idmoodle', $id)->delete();
			return redirect('admin/user')->with('success', 'Menghapus data user berhasil');
		}
    }
}
