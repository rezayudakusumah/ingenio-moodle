<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LmsEnrollments;
use App\Models\User;

class IndexController extends Controller
{
    public function index() {
        $pengajaraktif = LmsEnrollments::select('users.idmoodle', 'users.name', \DB::raw('COUNT(lms_enrollments.iduser) as total'))
						->join('users', 'lms_enrollments.iduser', '=', 'users.idmoodle')
						->groupBy('lms_enrollments.iduser')
						->orderBy('total', 'desc')
						->limit(5)
						->get();
						
		$pelajaraktif = LmsEnrollments::select('users.idmoodle', 'users.name', \DB::raw('COUNT(lms_enrollments.iduser) as total'))
						->join('users', 'lms_enrollments.iduser', '=', 'users.idmoodle')
						->groupBy('lms_enrollments.iduser')
						->orderBy('total', 'desc')
						->limit(5)
						->get();
						
		// $courseaktif = LmsEnrollment::join('lms_courses', 'lms_enrollments.idcourse', '=', 'lms_courses.idcourse')
		// 				->select('lms_courses.idcourse', 'lms_courses.fullname', \DB::raw('COUNT(lms_enrollments.idcourse) as total'))
		// 				->groupBy('lms_enrollments.idcourse')
		// 				->orderBy('total', 'desc')
		// 				->limit(5)
		// 				->get();

        $data = [
			'totaluser' => User::count(),
			'totalpengajar' => User::where('level', 'teacher')->count(),
			'totalpelajar' => User::where('level', 'user')->count(),
			// 'totalkelas' => LmsCourses::where('visible', '1')->count(),
			
			'pengajaraktif' => $pengajaraktif,
			'pelajaraktif' => $pelajaraktif,
			// 'courseaktif' => $courseaktif,
			
			// 'lastlogin' => $lastlogin,
			// 'hariini' => $hariini,
			// 'mingguini' => $mingguini,
			// 'bulanini' => $bulanini,
			// 'totallogin' => $totallogin,
		];

        return view('admin.index', $data);
    }
}
