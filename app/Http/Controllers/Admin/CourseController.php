<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LmsCategories;
use App\Models\LmsCourses;
use GuzzleHttp\Client;
use App\Models\LmsSettings;
use File;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
			'courses' => LmsCourses::orderBy('id', 'desc')->get(),
		];

		return view('admin.courses.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
			'category' => LmsCategories::get(),
		];

        return view('admin.courses.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $validateData = $request->validate([
			'idcategory' => 'required',
			'fullname' => 'required',
			'shortname' => 'required',
			'availability' => 'required',
			'model' => 'required',
			// 'video' => 'mimes:mp4,mov,ogg,qt | max:20000'
		]);
		
		if($request->visible == ""){
			$visible = "0";
		}else{
			$visible = $request->visible;
		}
		
		// dd(strtotime($request->startdate));
		$api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$function = 'core_course_create_courses';
		$client = new Client();
		
		$postCourse = $client->request('POST', $getapi->siteurl . '/webservice/rest/server.php?wstoken=' . $getapi->token . '&wsfunction='.$function.'&moodlewsrestformat=json',		
			array(
				'form_params' => array(
					'courses' => array(
						array(
							'fullname' => $request->fullname,
							'shortname' => $request->shortname,
							'categoryid' => $request->idcategory,
							'summary' => $request->summary,
							'visible' => $visible,
						)
					)
				)
			)
		);
		
		$dataCourse = $postCourse->getBody();
		$dataCourse = json_decode($dataCourse);
		
		if(isset($dataCourse->message)){
			return redirect()->back()->with('message', $dataCourse->message);
		}else{
			$course = new LmsCourses;
			
			// if($request->video != null){
			// 	$video = $request->video;
			// 	$video->move('storage/course/', $video->getClientOriginalName());
			// 	$video_name = $video->getClientOriginalName();
			// 	return $video_name;
            //     $course->video = $video_name;
			// }
			
			$course->idcategory = $request->idcategory;
			$course->idcourse = $dataCourse[0]->id;
			$course->fullname = $request->fullname;
			$course->shortname = $request->shortname;
			$course->summary = $request->summary;
			$course->visible = $visible;
			$course->availability = $request->availability;
			$course->model = $request->model;
			$course->price = $request->price;
			$course->accesscode = $request->accesscode;
			$course->topik = $request->topik;
			$course->tujuan = $request->tujuan;
			$course->durasi = $request->durasi;
			$course->level = $request->level;
            $course->start_date = $request->startdate;
            $course->end_date = $request->enddate;
			$course->rekomendasi = $request->rekomendasi;
			$course->save();
			
			if($course != null){
				return redirect('/admin/course');
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $data = [
			'course' => LmsCourses::where('idcourse', $id)->with('category')->first(),
			'category' => LmsCategories::get(),
		];
        
        return view('admin.courses.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validateData = $request->validate([
			'idcategory' => 'required',
			'fullname' => 'required',
			'shortname' => 'required',
			'availability' => 'required',
			'model' => 'required',
			// 'video' => 'mimes:mp4,mov,ogg,qt | max:20000'
		]);
		
		// dd($request);
		
		if($request->visible == ""){
			$visible = "0";
		}else{
			$visible = $request->visible;
		}
		
		$api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$course = LmsCourses::where('idcourse', $id)->first();
		
		$function = 'core_course_update_courses';
		$client = new Client();
		
		$postCourse = $client->request('POST', $getapi->siteurl . '/webservice/rest/server.php?wstoken=' . $getapi->token . '&wsfunction='.$function.'&moodlewsrestformat=json',		
			array(
				'form_params' => array(
					'courses' => array(
						array(
							'id' => $id,
							'fullname' => $request->fullname,
							'shortname' => $request->shortname,
							'categoryid' => $request->idcategory,
							'summary' => $request->summary,
						)
					)
				)
			)
		);
		
		$dataCourse = $postCourse->getBody();
		$dataCourse = json_decode($dataCourse);
		
		if(isset($dataCourse->message)){
			return redirect()->back()->with('message', $dataCourse->message);
		}else{

			// if($request->file('video') != null){
			// 	$video = $request->file('video');
			// 	$video->move('storage/course/', $video->getClientOriginalName());
			// 	$video_name = $video->getClientOriginalName();
				
            //     if(file_exists(public_path('storage/course/' . $video_name))){
            //         File::delete(public_path('storage/course/' . $video_name));
            //     }

			// }else{
			// 	$video_name = $course->video;
			// }
			
			$course->idcategory = $request->idcategory;
			$course->fullname = $request->fullname;
			$course->shortname = $request->shortname;
			$course->summary = $request->summary;
			$course->visible = $visible;
			$course->availability = $request->availability;
			$course->model = $request->model;
			$course->price = $request->price;
			$course->accesscode = $request->accesscode;
			$course->topik = $request->topik;
			$course->tujuan = $request->tujuan;
			// $course->video = $video_name;
			$course->durasi = $request->durasi;
			$course->level = $request->level;
			$course->rekomendasi = $request->rekomendasi;
			$course->save();
			
			return redirect('/admin/course');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $api = LmsSettings::get();
		foreach($api as $getapi){}
		
		$function = 'core_course_delete_courses';
		$client = new Client();
		
		$delCourse= $client->request('POST', $getapi->siteurl.'/webservice/rest/server.php?wstoken='.$getapi->token.'&wsfunction='.$function.'&moodlewsrestformat=json',
			array(
				'form_params' => array(
					'courseids' => array($id),
				)
			)
		);
		
		$dataDel = $delCourse->getBody();
		$dataDel = json_decode($dataDel);
		
		if(isset($dataDel->message)){
			return redirect()->back()->with('message', $dataDel->message);
		}else{
			$Course = LmsCourses::where('idcourse', $id)->first();
			if($Course->video != null){
				File::delete(public_path('storage/course/' . $Course->video));
			}
			$Course->delete();
			return redirect()->back()->with('success', 'Berhasil menghapus data course');
		}
    }
}
