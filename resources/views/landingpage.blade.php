@extends('layouts.ingenio-app')

@section('title', 'Home')

@push('style')
    <style>
        .ms-navbar.ms-navbar-app {
            position: fixed;
            width: 100%;
            background: transparent;
            border-bottom: 1px solid rgba(255, 255, 255, 0.2);
        }

        .ms-navbar.ms-navbar-app .navbar-collapse .navbar-nav .nav-item>a {
            color: #ffffff;
        }

        .ms-navbar.ms-navbar-app.fixed-top {
            background: #fff;
        }

        .ms-navbar.ms-navbar-app.fixed-top .navbar-collapse .navbar-nav .nav-item>a {
            color: #424242;
        }

        .ms-navbar.ms-navbar-app.fixed-top .navbar-collapse .navbar-nav .nav-item>a:hover {
            color: #ffffff;
        }

        .button-all {
            display: inline-block;
            font-size: 16px;
            font-weight: 500;
            color: #4ca3d9;
            text-transform: uppercase;
            border: 3px solid #4ca3d9;
            border-radius: 35px;
            padding: .75rem 3rem;
            transition: all 0.3s;
        }

        .button-all:hover {
            background: #4ca3d9;
            color: #fff;
        }

        .responsip {
            margin-bottom: 1rem;
        }

        @media (max-width: 767px) {
            .ms-navbar.ms-navbar-app .btn-navbar-menu {
                color: #fff;
            }

            .ms-navbar.ms-navbar-app.fixed-top .btn-navbar-menu {
                color: #333;
            }
        }
    </style>
@endpush

@section('content')
    <div class="bg-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><span>INGENIO BELAJAR ONLINE</span></h1>
                    <p>Selamat datang di situs ingenio belajar online</p>
                    <div class="header-link-wrap">
                        <a class="btn-header" href="#available">Jelajahi kelas</a>
                        <a class="btn-header-link" href="#" data-toggle="modal" data-target="#myModal">Gunakan kode kelas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap bg-white" id="available">
        <div class="container">
            <div class="row">
                <div class="col-md-12 no-p">
                    <div class="responsip">
                        <div class="card-panel">
                            <a href="/catalog/detail">
                                <img src="/assets/img/dummy.jpg" data-lazy="" alt="Sample Course V1">
                                <div class="card-block">
                                    <span class="badge badge-success">E-Learning</span>
                                    <p class="title">Sample Course V1</p>
                                    <p class="date" title="Start Date and End Date"><i class="far fa-calendar"></i>&nbsp;&nbsp;12/04/2021 - 12/05/2021</p>
                                    <p class="rating"><i class="fas fa-star" style="color: #FFB800;"></i> 3.4 penilaian</p>
                                    <div class="color-primary text-right mt-1">Gratis</div>
                                </div>
                            </a>
                        </div>
                        <div class="card-panel">
                            <a href="/catalog/detail">
                                <img src="/assets/img/dummy.jpg" data-lazy="" alt="Sample Course V2">
                                <div class="card-block">
                                    <span class="badge badge-danger">In Class</span>
                                    <p class="title">Sample Course V2</p>
                                    <p class="date" title="Start Date and End Date"><i class="far fa-calendar"></i>&nbsp;&nbsp;12/04/2021 - 12/05/2021</p>
                                    <p class="rating"><i class="fas fa-star" style="color: #FFB800;"></i> 4.2 penilaian</p>
                                    <div class="color-primary text-right mt-1">Rp 279.000</div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="text-center">
                        <a class="button-all" href="/catalog">Lihat semua</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog animated zoomIn animated-3x" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <h3 class="headline headline-sm m-0">Gunakan kode kelas</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="zmdi zmdi-close"></i>
                            </span>
                        </button>
                    </div>
                    <form id="CheckPassword" class="form-group no-m" action="/code/find" method="post">
						@csrf
                        <input id="CoursePassword" class="form-control" type="text" name="code" value="" placeholder="Masukkan kode kelas">
                        <input class="btn btn-raised btn-primary pull-right mb-0" type="submit" name="" value="Temukan">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
