<div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
    <div class="sb-slidebar-container">
        <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
            <li><a href="{{url('/')}}"><i class="fas fa-home"></i> Home</a></li>
            <li><a href="{{url('/catalog')}}"><i class="fas fa-th-large"></i> Katalog</a></li>
            @if(Auth::check())
                <li><a href="{{url('/my')}}"><i class="fas fa-bookmark"></i> Kelas Saya</a></li>
            @else
                <li><a href="{{url('/auth')}}"><i class="fas fa-sign-in-alt"></i> Masuk</a></li>
            @endif
            <li class="nav-item dropdown">
                <a href="#" class="dropdown-toggle d-flex align-items-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <img src="/assets/img/lang/{{ Config::get('app.locale') == 'id' ? 'id.png' : 'us.png' }}" alt="" style="width: 20px; margin-right: 15px;"> {{ Config::get('app.locale') == 'id' ? 'Indonesia' : 'English' }} <i class="zmdi zmdi-chevron-down ml-auto mr-0"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="" id="sidebarLangIndonesia"><img src="/assets/img/lang/id.png" alt="" style="width: 20px; margin-right: 15px;">Indonesia</a></li>
                    <li><a class="dropdown-item" href="" id="sidebarLangEnglish"><img src="/assets/img/lang/us.png" alt="" style="width: 20px; margin-right: 15px;">English</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
@push('script')
    <script>
        $('#sidebarLangEnglish').attr('href', `{{url('/')}}/lang?q=en&urldirect=${window.location.href}`);
        $('#sidebarLangIndonesia').attr('href', `{{url('/')}}/lang?q=id&urldirect=${window.location.href}`);
    </script>
@endpush