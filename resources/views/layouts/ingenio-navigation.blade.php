<nav class="navbar navbar-expand-md  navbar-static ms-navbar ms-navbar-app navbar-mode" style="margin-bottom:0;">
    <div class="container container-full">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <img src="/assets/img/Logo2.png">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="ms-navbar" style="margin-right: 0;">
            
            <ul class="navbar-nav">
                <li class="nav-item"><a href="/catalog">Katalog</a></li>
                @if(Auth::check())
                    <li class="nav-item"><a href="/my">Kelas Saya</a></li>
                    <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="/assets/img/avatar.svg" style="height: 25px; border-radius: 100%;" alt="fullname">
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="dropdown-item" style="background: transparent; font-weight: 600; color: #333 !important;">{{Auth::user()->name}}</a>
                            </li>
                            <li role="separator" class="dropdown-divider"></li>
                            <!-- <li>
                                <a class="dropdown-item" href="{{url('profile')}}">Profil</a>
                            </li> -->
                            <li>
                                <a class="dropdown-item" href="{{url('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Keluar</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @else
                <li class="nav-item"><a href="/">Masuk</a></li>
                @endif
            </ul>
            
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle animated fadeIn animation-delay-7" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="/assets/img/lang/{{ Config::get('app.locale') == 'id' ? 'id.png' : 'us.png' }}" alt="" style="width: 20px;"><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <a class="dropdown-item" href="" id="tombolIndonesia"><img src="/assets/img/lang/id.png" alt="" style="width: 20px;"> Indonesia</a>
                        <a class="dropdown-item" href="" id="tombolEnglish"><img src="/assets/img/lang/us.png" alt="" style="width: 20px;"> English</a>
                    </ul>
                </li>
            </ul>
        </div>
        <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu">
            <i class="fas fa-bars"></i>
        </a>
    </div>
</nav>

@push('script')
    <script>
        $('#tombolEnglish').attr('href', `{{url('/')}}/lang?q=en&urldirect=${window.location.href}`);
        $('#tombolIndonesia').attr('href', `{{url('/')}}/lang?q=id&urldirect=${window.location.href}`);
    </script>
    <script>
        $('#dropdown-toggle').click(function(){
            $('.dropdown-canvas#category-toggle').toggle(200);
        });
    </script>

    {{-- isi data query di input search --}}
    <script>
        let url_string = window.location.href; //window.location.href
        let url = new URL(url_string);
        let query = url.searchParams.get("q");
        $('#q').val(query);
    </script>
@endpush
