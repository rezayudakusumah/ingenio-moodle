<?php
	// dd(get_footer());
?>
<div class="wrap pt-2 pb-2 on-mobile-detail" style="background-color: #12100e;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 text-left" style="color: white;">
               
                <p style="margin: 0;">Milik Demo Site © 2019 PUSAT PENGEMBANGAN SISTEM INFORMASI</p>
                <div class="mt-2">
                    <p style="margin: 0;"><i class="fas fa-map-marker-alt"></i>&nbsp; Jl. Cijawura Girang II No. 22, Sekejati, Kec. Buahbatu, Kota Bandung (40286) Jawa Barat</p>
                    <p style="margin: 0;"><i class="fas fa-envelope"></i>&nbsp; info@dataquest.co.id</p>
                    <p style="margin: 0;"><i class="fas fa-phone"></i>&nbsp; (022) 2053 87500496</p>
                    <!-- <p style="margin: 0;"><i class="fas fa-fax"></i> - </p> -->
                </div>
            </div>
            <div class="col-md-4 text-right">
                <div class="row">
                    {{--
                        <div class="col">
                            <div>
                                <div class="dropdown">
                                    <a class="btn btn-white dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Language
                                    </a>
                                
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" id="tombolEnglish" href="">English</a>
                                    <a class="dropdown-item" id="tombolIndonesia" href="">Bahasa Indonesia</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    --}}
                    <div class="col">
                        <div>
                            <a href="#" class="btn-circle btn-circle-sm btn-circle-raised btn-facebook">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                            <a href="#" class="btn-circle btn-circle-sm btn-circle-raised btn-twitter">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#" class="btn-circle btn-circle-sm btn-circle-raised btn-instagram">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>