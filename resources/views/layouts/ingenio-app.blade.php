<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#333">
        <title>Ingenio | @yield('title')</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="url_base" content="{{ url('') }}">
        <meta name="host_id" content="{{empty(Auth::user()) ? '' : Auth::user()->id}}">
        <meta name="app-parents" content=".">

        <link rel="stylesheet" href="/assets/sweetalert2/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/fontawesome/css/all.min.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>

        <meta name="description" content="Ingenio Belajar Online">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Indie+Flower:400|">
        <link rel="stylesheet" href="/assets/css/preload.min.css">
        <link rel="stylesheet" href="/assets/css/plugins.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/ingeniostyle.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/addon-style.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/css/addon-mobile-style.css"/>
        <link rel="shotcut icon" href="/assets/img/favicon.png">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500&family=Poppins:wght@400;500;700;800&display=swap" rel="stylesheet">

        {{-- awal script pusher --}}
        @if (Auth::user())
            <script>
                var userId = "{{Auth::user()->idmoodle}}";
            </script>
            <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
            <script>
                function notifikasi(judul = 'Pemberitahuan', body = 'default', url='#') {
                    if (!Notification) {
                        alert('Browsermu tidak mendukung Web Notification.'); 
                        return;
                    }
                    if (Notification.permission !== "granted")
                        Notification.requestPermission();
                    else {
                        var notifikasi = new Notification(judul, {
                            icon: '{{ asset("assets/img/smile.png") }}',
                            body: body,
                        });
                        notifikasi.onclick = function () {
                            window.open(url);      
                        };
                        setTimeout(function(){
                            notifikasi.close();
                        }, 5000);
                    }
                };
                
                // Enable pusher logging - don't include this in production
                Pusher.logToConsole = false;

                var pusher = new Pusher('d3c2fb7bf8efe79c9b10', {
                    cluster: 'ap1',
                    forceTLS: true
                });

                var channel = pusher.subscribe('smile-channel');
                channel.bind('smile-event', function(data) {
                    let d = JSON.stringify(data.message);
                    d = JSON.parse(d);
                    d = JSON.parse(d);

                    let pesan = d.message;
                    let judul = d.title;
                    let link = d.link;

                    if(d.user){
                        d.user.find((id) => {
                            if(id === parseInt("{{Auth::user()->idmoodle}}")){
                                notifikasi(judul, pesan, link);
                            }
                        })
                    }else{
                        notifikasi(judul, pesan, link);
                    }
                });
            </script>
        @endif
        {{-- akhir script pusher --}}

        <style>
            .dq-project-bg{
                background-image: url(/img/project-bg.jpg);
                background-size: cover;
                background-position: top center;
                background-repeat: no-repeat;
            }
            .course-live{
                position:absolute !important;
                z-index:999;
                margin:10px 0 0 10px;
            }

            .ms-navbar{
                height: 85px;
            }
        </style>
        @stack('style')
    </head>
    <body>
        <div id="ms-preload" class="ms-preload">
            <div id="status">
                <div class="spinner">
                    <div class="dot1"></div>
                    <div class="dot2"></div>
                </div>
            </div>
        </div>

        <div class="ms-site-container">
            @include('layouts.ingenio-navigation')

            @yield('content')

            @include('layouts.ingenio-footer')

            <script src="/assets/js/all.js"></script>
            <script type="text/javascript">
                $("#ChangeLanguage").change(function(){
                    //alert($(this).val())
                    $.ajax({
                        url : "/change-language",
                        method : "post",
                        data : {'locale':$(this).val(), _token : '{{ csrf_token() }}'},
                        success: function(result){
                            location.reload();
                        }
                    })
                })
            </script>

            <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
            <script type="text/javascript">
                $('.headerFade').slick({
                    dots: false,
                    infinite: true,
                    speed: 900,
                    fade: true,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    arrows: false,
                    cssEase: 'linear'
                });
                $('.responsips').slick({
                    dots: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 3.15,
                    slidesToScroll: 1,
                    responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                        slidesToShow: 2.65,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: false
                        }
                    },
                    {
                        breakpoint: 995,
                        settings: {
                        slidesToShow: 2.63,
                        slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                        slidesToShow: 2.1,
                        slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                        slidesToShow: 2.25,
                        slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 2.2,
                        slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                    ]
                });
                $('.responsip').slick({
                    dots: false,
                    infinite: false,
                    speed: 600,
                    slidesToShow: 4.3,
                    slidesToScroll: 3,
                    responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                        slidesToShow: 2.25,
                        slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1.35, // Default 2.2
                        slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                    ]
                });

                $('.lazy').slick({
                    lazyLoad: 'ondemand',
                    slidesToShow: 4,
                    slidesToScroll: 1,
                });

                $('#dropdown-toggle').click(function(){
                    $('.dropdown-canvas#category-toggle').toggle(200);
                });
            </script>
        </div>

        @include('layouts.ingenio-sidebar')
        @stack('script')
    </body>
</html>
