@extends('admin.layouts.admin-app')

@section('title', 'List Users')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">List Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
                @if(session()->has('success'))
                  <div class="alert alert-success">
                    {{ session()->get('success') }}
                  </div>
                @endif
                <table id="datatables" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>Photo</th>
                      <th>Account Type</th>
                      <th>Level</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($user as $n => $getuser)
                      <tr>
                        <td>{{$n+1}}</td>
                        <td>{{$getuser->name}}</td>
                        <td>{{$getuser->email}}</td>
                        <td>
                          <img src="{{$getuser->photo}}" style="height: 50px">
                        </td>
                        <td>{{$getuser->accounttype}}</td>
                        <td><a href="/admin/change/level/{{$getuser->id}}">{{ucwords($getuser->level)}}</a></td>
                        <td>
                          @if($getuser->email_verified_at==null)
                            <a href="{{url('/admin/user/verify/'.md5($getuser->id))}}" onclick="return confirm('Anda yakin akan verifikasi data ini?')" class="badge badge-primary">Verifikasi akun</a>
                          @else
                          Verified
                          @endif
                        </td>
                        <td>
                          <a class="btn btn-info btn-sm" href="/admin/user/detail/{{$getuser->id}}">Detail</a>
                          &nbsp;
                          @if($getuser->idmoodle != null)
                            <a class="btn btn-warning btn-sm" href="/admin/user/{{$getuser->idmoodle}}/edit" title="Edit"><i class="fas fa-edit"></i></a>
                          @endif
                          &nbsp;
                          @if($getuser->idmoodle != null)
                          <form action="{{ route('user.destroy', $getuser->idmoodle) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete this course?')"><i class="fas fa-trash"></i></button>
                          </form>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>Photo</th>
                      <th>Account Type</th>
                      <th>Level</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@push('script')
  <script>
    $(function () {
      $('#datatables').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endpush