@extends('admin.layouts.admin-app')

@section('title', 'Update Users')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Update Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
	
    <!-- /.content-header -->
	<div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 mx-auto">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
                @if($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <form action="/admin/user/{{$users->idmoodle}}" method="post" enctype="multipart/form-data">
                    @method('PUT')
					{{ csrf_field() }}
					<div class="form-group row">
						<label class="col-md-3 col-form-label text-md-right">Username</label>
						<div class="col-md-7">
						  <input class="form-control" type="text" name="username" value="{{$users->username}}" placeholder="">
						</div>
					  </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label text-md-right">Fullname</label>
						<div class="col-md-7">
						  <input class="form-control" type="text" name="fullname" value="{{$users->name}}" placeholder="">
						</div>
					  </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label text-md-right">Email</label>
						<div class="col-md-7">
						  <input class="form-control" type="text" name="email" value="{{$users->email}}" placeholder="">
						</div>
					  </div>
					<div class="form-group row">
						<label class="col-md-3 col-form-label text-md-right">Password</label>
						<div class="col-md-7">
						  <input class="form-control" type="password" name="password" placeholder="">
						</div>
					  </div>
					  <input type="hidden" name="idmoodle" value="{{$users->idmoodle}}">
					  <div class="form-group row" style="margin-bottom: 0;">
                    <div class="col-md-7 offset-md-3">
                      <input class="btn btn-primary" type="submit" value="Save">
                    </div>
                  </div>
				</form>
              </div>
            </div><!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection