@extends('admin.layouts.admin-app')

@section('title', 'List Category Course')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">List Category Course</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List Category Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
	
	  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
                <table id="datatables" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Parent</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Visible</th>
                      <th>Course Count</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
				          @foreach($category as $getcat)
                    <tr>
                      <td>{{$getcat->parent}}</td>
                      <td>{{$getcat->name}}</td>
                      <td>{{$getcat->description}}</td>
                      <td>{{$getcat->visible}}</td>
                      <td>{{$getcat->coursecount}}</td>
                      <td>
                        <a class="btn btn-sm btn-warning" href="/admin/coursecategory/edit/{{$getcat->idcategory}}" title="Edit"><i class="fas fa-edit"></i></a>
                        &nbsp;
                        <a href="/admin/coursecategory/delete/{{$getcat->idcategory}}" class="btn btn-sm btn-danger" title="Delete" onclick="return confirm('Are you sure want to delete this category?')"><i class="fas fa-trash"></i></a>
                      </td>
                    </tr>
				          @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>Parent</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Visible</th>
                      <th>Course Count</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@push('script')
  <script>
    $(function () {
      $('#datatables').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endpush