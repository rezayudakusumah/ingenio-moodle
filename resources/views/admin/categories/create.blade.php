@extends('admin.layouts.admin-app')

@section('title', 'Create Category Course')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Category Course</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create Category Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
	
	  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6 mx-auto">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
                @if($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <form action="/admin/category" method="post">
                  {{ csrf_field() }}
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Parent</label>
                    <div class="col-md-7">
                      <select class="select2" data-placeholder="Select Parent" name="parent" style="width: 100%;">
                        <option value="0">Top</option>
                        @foreach($category as $getcat)
                          <option value="{{$getcat->idcategory}}">{{$getcat->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
				          <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Name</label>
                    <div class="col-md-7">
                      <input class="form-control" type="text" name="name" placeholder="" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Description</label>
                    <div class="col-md-7">
                      <textarea class="form-control" rows="3" name="description" placeholder=""></textarea>
                    </div>
                  </div>
				          <div class="form-group row" style="margin-bottom: 0;">
                    <div class="col-md-7 offset-md-3">
                      <input class="btn btn-primary" type="submit" value="Save">
                    </div>
                  </div>
				        </form>
              </div>
			      </div>
			    </div>
		    </div>
		  </div>
    </div>
	</div>
@endsection

@push('script')
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()
    });
  </script>
@endpush