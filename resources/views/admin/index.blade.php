@extends('admin.layouts.admin-app')

@section('title', 'Home Admin')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

  	@if(Session::has('success_change'))
		<div class="alert alert-info text-center">
		{{ Session::get('success_change') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
		</div>
	@endif
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard Admin</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-3">
                <div class="card bg-primary text-center">
					<div class="card-header">Total User</div>
					<div class="card-body">
						<strong>{{$totaluser}}</strong>
					</div>
					<a href="/admin/user/list">
						<div class="card-footer">Lihat</div>
					</a>
                </div>
            </div>
			<div class="col-md-3">
                <div class="card bg-success text-center">
					<div class="card-header">Total Pengajar</div>
					<div class="card-body">
						<strong> {{$totalpengajar}} </strong>
					</div>
					<a href="/admin/user/list">
						<div class="card-footer">Lihat</div>
					</a>
                </div>
            </div>
			<div class="col-md-3">
                <div class="card bg-info text-center">
					<div class="card-header">Total Pelajar</div>
					<div class="card-body">
						<strong>  {{$totalpelajar}} </strong>
					</div>
					<a href="">
						<div class="card-footer">Lihat</div>
					</a>
                </div>
            </div>
			<div class="col-md-3">
                <div class="card bg-warning text-center">
					<div class="card-header">Total Kelas</div>
					<div class="card-body">
						<strong>  1000 </strong>
					</div>
					<a href="">
						<div class="card-footer">Lihat</div>
					</a>
                </div>
            </div>
			<div class="col-md-4">
				<div class="card">
				  <div class="card-header bg-secondary">
					Total Pengunjung Smile
				  </div>
				  <ul class="list-group list-group-flush">
					<li class="list-group-item">Hari Ini <span class="badge badge-info float-right">1000</span></li>
					<li class="list-group-item">Minggu Ini <span class="badge badge-info float-right">1000</span></li>
					<li class="list-group-item">Bulan Ini <span class="badge badge-info float-right">1000</span></li>
					<li class="list-group-item">Total Login <span class="badge badge-info float-right">1000</span></li>
				  </ul>
				  <div class="card-footer"><a href="/admin/user/log">Selengkapnya</a></div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="card">
					<div class="card-header bg-secondary">Last Access Login</div>
					<div class="card-body">
						<div id="columnActiveTeachersDaily" class="table-responsive">
							<table class="table table-bordered">
							  <tr>
								<th>No</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Last Access</th>
							  </tr>
							  
							</table>
                      </div>
					</div>
                </div>
			</div>
			
			<div class="col-md-4" style="display: none;">
				<div class="card">
					<div class="card-header bg-success text-center">Pengajar Aktif</div>
					<div class="card-body">
						<div id="columnActiveTeachersDaily" class="table-responsive">
							<table class="table table-bordered">
							  <tr>
								<th>No</th>
								<th>Nama</th>
								<th>Total Course</th>
							  </tr>
							  @foreach($pengajaraktif as $index => $data)
							  <tr>
								<td>{{$index +1}}</td>
								<td>{{$data->name}}</td>
								<td>{{ $data->total }}</td>
							  </tr>
							  @endforeach
							</table>
                      </div>
					</div>
                </div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header bg-info text-center">Pelajar Aktif</div>
					<div class="card-body">
						<div id="columnActiveTeachersDaily" class="table-responsive">
							<table class="table table-bordered">
							  <tr>
								<th>No</th>
								<th>Nama</th>
								<th>Total Course</th>
							  </tr>
							  @foreach($pelajaraktif as $index => $data)
							  <tr>
								<td>{{$index +1}}</td>
								<td>{{$data->name}}</td>
								<td>{{ $data->total }}</td>
							  </tr>
							  @endforeach
							</table>
                      </div>
					</div>
                </div>
				
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header bg-warning text-center">Course Aktif</div>
					<div class="card-body">
						<div id="columnActiveTeachersDaily" class="table-responsive">
							<table class="table table-bordered">
							  <tr>
								<th>No</th>
								<th>Nama</th>
								<th>Total User</th>
							  </tr>
							  
							</table>
                      </div>
					</div>
				</div>
			</div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@push('script')
    <script>
    $(function () {
      $('#datatables').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endpush