<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  
  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- User Dropdown Menu -->
    <li class="nav-item dropdown dropdown-user">
      {{-- <a class="nav-link" data-toggle="dropdown" href="#">
        <img src="/assets/admin/img/user2-160x160.jpg" class="img-circle elevation-1" alt="User Image">
        <p>Alexander Pierce</p>
      </a> --}}

      <a href="#" class="nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="/assets/img/Logo3.png" class="img-circle elevation-1" alt="{{Auth::user()->name}}">
        {{Auth::user()->name}}
      </a>

      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <div class="user-header">
          {{-- <img src="/assets/img/Logo2.png" class="img-circle" alt="User Image">
          <p>
            Alexander Pierce - Web Developer
            <small>Member since Nov. 2012</small>
          </p> --}}

          <img src="/assets/img/Logo3.png" class="img-circle" alt="{{Auth::user()->name}}">
          <p>
            {{Auth::user()->name}}
          </p>

        </div>
        <div class="user-footer">
          <div class="mr-auto">
            <a href="" class="btn btn-default btn-flat">Profile</a>
          </div>
          <div class="ml-auto">
            <a href="{{url('logout')}}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </div>
        </div>
      </div>
    </li>
  </ul>
</nav>
<!-- /.navbar -->