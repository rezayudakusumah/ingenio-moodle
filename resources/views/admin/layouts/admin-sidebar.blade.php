<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="/" class="brand-link">
		<img src="/assets/img/Logo3.png" alt="AdminLTE Logo" class="brand-image" style="float: none;">
	</a>

	<div class="sidebar">
		<nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                
                <li class="nav-item">
                    <a href="/admin/dashboard" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i><p>Home</p>
                    </a>
                </li>

                <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder-open"></i>
                    <p>
                    Category
                    <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="/admin/category/create" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Create Category</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="/admin/category" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>List Category</p>
                    </a>
                    </li>
                </ul>
                </li>
            
                <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-folder-open"></i>
                    <p>
                    Course
                    <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="/admin/course/create" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Create Course</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="/admin/course" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>List Course</p>
                    </a>
                    </li>
                </ul>
                </li>

                <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                    User
                    <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="/admin/user/create" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Create User</p>
                    </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/user" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>List Users</p>
                        </a>
                    </li>
                </ul>
                </li>

                <li class="nav-item">
                    <a href="/moodle/admin" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i><p>Menuju Moodle</p>
                    </a>
                </li>
            </ul>
        </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>