<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('title')</title>

    <!-- Select2 -->
    <link rel="stylesheet" href="/assets/admin/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="/assets/admin/plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/assets/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/assets/admin/css/adminlte.min.css">
    <link rel="stylesheet" href="/assets/admin/css/addon.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="/assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
	<link rel="stylesheet" href="/assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="/assets/admin/plugins/summernote/summernote-bs4.css">
    @stack('style')
  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">

        @include('admin.layouts.admin-navigation')
        @include('admin.layouts.admin-sidebar')

      @yield('content')

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Milik Demo Site © 2019 PUSAT PENGEMBANGAN SISTEM INFORMASI
      </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="/assets/admin/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- InputMask -->
    <script src="/assets/admin/plugins/moment/moment.min.js"></script>
    <script src="/assets/admin/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Select2 -->
    <script src="/assets/admin/plugins/select2/js/select2.full.min.js"></script>
    <!-- DataTables -->
    <script src="/assets/admin/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/assets/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <!-- AdminLTE App -->
    <script src="/assets/admin/js/adminlte.min.js"></script>
    <!-- jQuery Knob -->
    <script src="/assets/admin/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- Sparkline -->
    <script src="/assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- ChartJS -->
    <script src="/assets/admin/plugins/chart.js/Chart.min.js"></script>
    {{-- sweatalert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	
	<script src="/assets/admin/js/bs-custom-file-input.min.js"></script>
    
	<script src="/assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
	
	<script src="/assets/admin/plugins/summernote/summernote-bs4.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
    @stack('script')
  </body>
</html>
