@extends('admin.layouts.admin-app')

@section('title', 'Create Course')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Course</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create Course</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
                @if($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                <form action="/admin/course" method="post">
					        {{ csrf_field() }}
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Category</label>
                    <div class="col-md-8">
                      <select class="select2" data-placeholder="Select category" name="idcategory" style="width: 100%;">
                        @foreach($category as $getcat)
                          <option value="{{$getcat->idcategory}}">{{$getcat->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Fullname</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="fullname" placeholder="" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Shortname</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="shortname" placeholder="" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Summary</label>
                    <div class="col-md-8">
                      <textarea class="form-control" rows="3" placeholder="" name="summary"></textarea>
                    </div>
                  </div>

                  <hr>
                  
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Topik</label>
                    <div class="col-md-8">
                      <textarea id="summernoteTopik" class="form-control" rows="3" placeholder="" name="topik"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Tujuan</label>
                    <div class="col-md-8">
                      <textarea id="summernoteTujuan" class="form-control" rows="3" placeholder="" name="tujuan"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                  <label class="col-md-3 col-form-label text-md-right">Video Teaser</label>
						      <div class="col-md-8 custom-file">
                      <input type="file" class="custom-file-input" id="customFile" name="video">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
				  
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Durasi</label>
                    <div class="col-md-1">
                      <input class="form-control" type="text" name="durasi" placeholder="" required/>
                    </div>
                    <label class="col-md-1 col-form-label text-md-right">Level</label>
                    <div class="col-md-2">
                      <select class="select2" data-placeholder="Select category" name="level" style="width: 100%;">
                        <option value="Easy">Easy</option>
                        <option value="Medium">Medium</option>
                        <option value="Intermediate">Intermediate</option>
                      </select>
                    </div>
                    <label class="col-md-2 col-form-label text-md-right">Rekomendasi</label>
                    <div class="col-md-2">
                      <input class="form-control" type="text" name="rekomendasi" placeholder="" required/>
                    </div>
                  </div>
				  
				          <hr>

                  <div class="form-group row">
                    <label class="col-md-3 text-md-right">Visibility</label>
                    <div class="col-md-8 custom-control custom-switch" style="padding-left: 2.75rem;">
                      <input type="checkbox" class="custom-control-input" id="visibility" name="visible" value="1" checked>
                      <label class="custom-control-label" for="visibility">Visible</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Start Date</label>
                    <div class="col-md-8">
                        <input type="date" name="startdate" class="form-control" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">End Date</label>
                    <div class="col-md-8">
                        <input type="date" name="enddate" class="form-control" required/>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 text-md-right">Availability</label>
                    <div class="col-md-8">
                      <select class="select2" name="availability" data-placeholder="Select model" style="width: 100%;">
                        <option value="Umum">Umum</option>
                        <option value="Private">Private</option>
                      </select>
					          </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Model</label>
                    <div class="col-md-8">
                      <select class="select2" name="model" data-placeholder="Select model" style="width: 100%;">
                        <option value="1">Elearning</option>
                        <option value="2">In Class</option>
						            <option value="3">Blended Learning</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Price</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="price" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Accesscode</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="accesscode" placeholder="">
                    </div>
                  </div>
				  {{--
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Topik</label>
                    <div class="col-md-8">
                      <input class="form-control" type="text" name="topik" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Durasi</label>
                    <div class="col-md-8">
                      <input class="form-control" type="number" name="durasi" placeholder="">
                    </div>
                    <span>
                    Jam
                    </span>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Level</label>
                    <div class="col-md-8">
                      <input class="form-control" type="number" name="level" placeholder="">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label text-md-right">Rekomendasi</label>
                    <div class="col-md-8">
                      <input class="form-control" type="number" name="rekomendasi" placeholder="">
                    </div>
                    <span>
                    Jam
                    </span>
                  </div>
				  --}}
                  <div class="form-group row" style="margin-bottom: 0;">
                    <div class="col-md-8 offset-md-3">
                      <input class="btn btn-primary" type="submit" value="Save Now">
                    </div>
                  </div>
                </form>
              </div>
            </div><!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@push('script')
  <script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()

      //Datemask dd/mm/yyyy
      $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
      $('[data-mask]').inputmask()
    });

  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
  </script>
@endpush