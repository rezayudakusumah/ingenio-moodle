@extends('admin.layouts.admin-app')

@section('title', 'List Courses')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">List Courses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List Courses</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                @if(session()->has('message'))
                  <div class="alert alert-warning">
                    {{ session()->get('message') }}
                  </div>
                @endif
				<div class="table-responsive">
					<table id="datatables" class="table table-bordered table-striped">
					  <thead>
						<tr>
						  <th>ID</th>
						  <th>Category</th>
						  <th>Fullname</th>
						  <th>Shortname</th>
						  <th>Visible</th>
						  <th>Date</th>
						  <th>Availability</th>
						  <th>Model</th>
						  <th>Price</th>
						  <th>Code</th>
						  <th>Visible</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
						@foreach($courses as $getcourse)
						  <tr>
							<td>{{$getcourse->idcourse}}</td>
							<td>{{get_name_category($getcourse->idcategory)}}</td>
							<td>{{$getcourse->fullname}}</td>
							<td>{{$getcourse->shortname}}</td>
							<td>{{$getcourse->visible}}</td>
							<td>{{$getcourse->start_date}} s.d {{$getcourse->end_date}}</td>
							<td>{{$getcourse->availability}}</td>
							<td>
							  @if($getcourse->model == '1')
								Elearning
							  @elseif($getcourse->model == '2')
								In Class
							  @elseif($getcourse->model == '3')
								Blended Learning
							  @endif
							</td>
							<td>{{$getcourse->price}}</td>
							<td>{{$getcourse->accesscode}}</td>
							<td>@if($getcourse->visible == '1')
									Show
								@else
									Hidden
								@endif
							</td>
							<td class="flex">
								<div class="btn-group ">
									<button type="button" class="btn btn-warning btn-xs">Action</button>
									<button type="button" class="btn btn-warning dropdown-toggle dropdown-icon" data-toggle="dropdown"></button>
									<span class="sr-only">Toggle Dropdown</span>
									<div class="dropdown-menu" role="menu">
										<a class="dropdown-item" href="/admin/enrolment/add/{{$getcourse->idcourse}}">Enroll</a>
										<a class="dropdown-item" href="/admin/course/moodle/{{$getcourse->idcourse}}">To Moodle</a>
										<a class="dropdown-item" href="/admin/course/certificate/{{$getcourse->idcourse}}">Certificate</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="/admin/course/{{$getcourse->idcourse}}/edit">Edit</a>
										<form action="{{ route('course.destroy', $getcourse->idcourse) }}" method="POST">
											@method('DELETE')
											@csrf
											<button class="dropdown-item" onclick="return confirm('Are you sure want to delete this course?')">Delete</button>
										</form>
									</div>
								</div>
							</td>
						  </tr>
						@endforeach
					  </tbody>
					</table>
				</div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@push('script')
  <script>
    $(function () {
      $('#datatables').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
      });
    });
  </script>
@endpush