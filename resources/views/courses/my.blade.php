@extends('layouts.ingenio-app')

@section('title', 'My Open Class')

@push('style')
    <style>
        body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4 {
            font-family: poppins;
            color: #000;
        }

        .btn.disabled {
            pointer-events: none;
        }

        .card--link {
            display: flex; 
            color: #333; 
            margin-bottom: 2rem;
        }

        .card--link.card--link-certificate {
            display: block; 
            margin-bottom: 1rem;
        }

        .card--link .card {
            background: #f9f9f9; 
            border: 1px solid #eee;
            border-radius: 10px;
            box-shadow: none;
            margin-bottom: 0;
            transition: all 0.2s;
        }

        .card--link.card--link-certificate .card {
            display: flex; 
            align-items: center; 
            flex-direction: row;
        }

        .card--link .card:hover {
            background: #f3f3f3; 
        }

        .card--link .card img {
            width: 100%; 
            height: 143px; 
            object-fit: cover;
            border-radius: 10px 10px 0 0;
        }

        .card--link.card--link-certificate .card img {
            width: 100px; 
            height: 100px; 
            padding: 1.25rem; 
            margin-right: -1.25rem;
        }

        .card--link.card--link-certificate .card .card-block {
            width: calc(100% - 87.5px); 
            height: 100px;
        }

        .card--link .card .card-block .title {
            font-size: 14px; 
            font-weight: 600; 
            color: #333; 
            line-height: 1.5; 
            margin-top: 0; 
            margin-bottom: 0;
        }

        .card--link .card .card-block .tutor,
        .card--link .card .card-block .rating,
        .card--link .card .card-block .progress-text,
        .card--link.card--link-certificate .card .card-block p {
            font-size: 13px; 
            font-weight: 500; 
            line-height: 1.5; 
            color: #999;
        }

        .card--link .card .card-block .tutor,
        .card--link.card--link-certificate .card .card-block p {
            margin-bottom: 0;
        }

        .card--link .card .card-block .rating i {
            color: gold;
        }

        .card--link .card .card-block .progress {
            height: 3px; 
            margin-bottom: 0;
        }

        .card--link .card .card-block .progress-text {
            margin-bottom: 0;
        }

        .item--certificate {
            display: block;
            font-size: 14px; 
            color: #999; 
            text-align: center;
            border: 2px solid #c3c3c3; 
            padding: .5rem 1rem;
            transition: all 0.2s;
        }

        .item--certificate:hover {
            background: #ff6347;
            color: #fff;
            border-color: #ff6347;
        }

        .my-class--access {
            background-color: #F2BB05; 
            background-image: url('/assets/img/mooc/avatar.svg'); 
            background-size: 200px;
            background-repeat: no-repeat;
            background-position: left;
            color: #fff; 
            height: 150px; 
            display: flex; 
            align-items: center; 
            justify-content: space-between; 
            border-radius: 10px; 
            padding: 2rem 2rem 2rem 25rem; 
            margin-bottom: 5rem;
        }

        .my-class--access .access-headline {
            font-size: 24px; 
            font-weight: 500;
            margin-right: 1rem;
            margin-bottom: 0;
        }

        .my-class--access .access-button {
            background: #fff; 
            font-size: 16px; 
            font-weight: 500; 
            color: #000; 
            border-radius: 5px; 
            box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.1);
            padding: 1rem 2rem;
            transition: all 0.3s;
        }

        .my-class--access .access-button:hover {
            background: #f9f9f9;
            box-shadow: none;
        }

        .my-class--access .access-button span.desktop-button-text {
            display: block;
        }

        .my-class--access .access-button span.mobile-button-text {
            display: none;
        }

        .my-class--badge {
            background: #fcfcfc; 
            color: #333; 
            display: flex; 
            flex-direction: column; 
            align-items: center; 
            text-align: center;
            border: 1px solid #f3f3f3; 
            border-radius: 10px; 
            cursor: pointer;
            padding: 2rem;
            margin-bottom: 2rem;
            transition: all 0.3s;
        }

        .my-class--badge:hover {
            box-shadow: 0px 5px 15px -5px rgb(0 0 0 / 10%);
        }

        .my-class--badge .badge-image {
            width: 100px; 
            height: 100px;
        }

        .my-class--badge .badge-headline {
            width: 100%;
            font-size: 16px; 
            font-weight: 700; 
            margin-bottom: 0;
        }

        .badge-detail--wrap .badge-headline {
            text-align: center;
        }

        .badge-detail--wrap .badge-headline img {
            width: auto; 
            height: 100px; 
            margin-bottom: 2rem;
        }

        .badge-detail--wrap .badge-headline .headline--badge {
            font-size: 24px; 
            font-weight: 700; 
            line-height: 1.5;
            margin-bottom: 0;
        }

        .badge-detail--wrap .badge-headline .headline--course {
            font-size: 14px; 
            font-weight: 500; 
            color: #bbb; 
            line-height: 1.5;
            letter-spacing: 1px; 
            margin-bottom: 0;
        }

        .badge-detail--wrap .badge-headline .headline--description {
            font-size: 14px; 
            font-weight: 500; 
            color: #333; 
            line-height: 1.5;
            margin-bottom: 0;
        }

        .badge-detail--wrap .table {
            font-size: 14px;
        }

        .nav-tab--wrap {
            position: sticky;
            background: #ff6347;
            transition: all 0.3s;
            border: 1px solid #f5f5f5;
            margin-bottom: 1rem;
            top: 100px;
        }

        .nav-tab--wrap .nav-tab--body {
            padding: 3rem;
        }

        .nav-tab--wrap .nav-tabs-ver {
            flex-direction: column;
        }

        .nav-tab--wrap .nav-tabs-ver li a {
            color: #fff;
            padding: 1rem 2rem;
            transition: all 0.2s;
        }

        .nav-tab--wrap .nav-tabs-ver li a:hover,
        .nav-tab--wrap .nav-tabs-ver li a.active {
            background: #f55336;
            color: #fff;
        }

        .headline-tab {
            font-size: 24px; 
            font-weight: 400; 
            margin-top: 0; 
            margin-bottom: 2rem;
        }

        .handler {
            display: block; 
            text-align: center;
        }

        .handler img {
            width: 200px; 
            margin-bottom: 2rem;
        }

        .handler .handler--headline {
            font-size: 24px; 
            font-weight: 700; 
            margin-bottom: 0;
        }

        .handler .handler--content {
            font-size: 14px; 
            font-weight: 400; 
            color: #999; 
            margin-bottom: 0;
        }

        .modal-rating {
            text-align: center;
        }

        .modal-rating .rating-headline {
            font-size: 18px; 
            font-weight: 700; 
            margin-bottom: 0;
        }

        .modal-rating .rating-text {
            font-size: 14px;
        }

        .modal-rating .rating-wrap {
            font-size: 24px; 
            margin-top: 2rem;
        }

        .fa-star-checked {
            color: orange;
        }

        .fa-star {
            cursor: pointer;
        }

        @media (max-width: 767px) {
            .my-class--access {
                background-image: none;
                background-size: 0;
                padding: 2rem;
            }

            .my-class--access .access-headline {
                font-size: 20px;
            }

            .my-class--access .access-button span.desktop-button-text {
                display: none;
            }

            .my-class--access .access-button span.mobile-button-text {
                display: block;
            }
        }

        @media (max-width: 420px) {
            .my-class--access .access-headline {
                font-size: 16px;
            }
        }
    </style>
@endpush

@section('content')
    <div class="bg-page-title">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2 class="headline-md no-m">Kelas Saya</h2>
            </div>
        </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap bg-white">
        <div class="container">
            {{-- Jika akun pengguna terdaftar di SMILE reguler, maka aktifkan section ini --}}
            <div class="my-class--access">
                <p class="access-headline">Akun anda terdaftar di SMILE reguler</p>
                <a class="access-button" href="/course/mycourse">
                    <span class="desktop-button-text">Akses SMILE Reguler</span>
                    <span class="mobile-button-text">Akses</span>
                </a>
            </div>
            {{-- Jika akun pengguna terdaftar di SMILE reguler, maka aktifkan section ini --}}
            
			@if(session()->has('message'))
				<div class="alert alert-royal alert-light alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <i class="zmdi zmdi-close"></i>
					</button>
					{{ session()->get('message') }}
				</div>
			@endif
			
            <div class="row">
                @if(count($courses) == 0)
                    <div class="col-md-12">
                        <div class="alert alert-primary">Anda belum memiliki kelas. Untuk mengikuti kelas, silahkan buka menu Catalog.</div>
                    </div>
                @else
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="myClass">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="headline-tab">Kelas saya</h2>
                                    </div>

                                    @foreach($courses as $get)
                                        <div class="col-md-4">
                                            <a class="card--link" href="/course/moodle/{{$get->idcourse}}"> {{-- Jika sudah memberi rating, maka gunakan link seperti baris ini --}}
                                                <div class="card">
                                                    <img src="/assets/img/dummy.jpg" alt="">
                                                    <div class="card-block">
                                                        <h3 class="title text-truncate">{{$get->course->fullname}}</h3>
                                                        <p class="tutor">OSL</p>
                                                        <p class="rating"><i class="fas fa-star"></i> 0.0 (0 Penilaian)</p>                                                        
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#modal-close').submit(function(e) {
            e.preventDefault();
            // Coding
            $('#IDModal').modal('toggle'); //or  $('#IDModal').modal('hide');
            return false;
        });
    </script>

    @foreach($courses as $get)
        <script>
            $('#ratingAlert-{{ $get->idcourse }} .fa-star-1').click(function(){ratingChecked{{ $get->idcourse }}(2)})
            $('#ratingAlert-{{ $get->idcourse }} .fa-star-2').click(function(){ratingChecked{{ $get->idcourse }}(3)})
            $('#ratingAlert-{{ $get->idcourse }} .fa-star-3').click(function(){ratingChecked{{ $get->idcourse }}(4)})
            $('#ratingAlert-{{ $get->idcourse }} .fa-star-4').click(function(){ratingChecked{{ $get->idcourse }}(5)})
            $('#ratingAlert-{{ $get->idcourse }} .fa-star-5').click(function(){ratingChecked{{ $get->idcourse }}(6)})
            let nilaiRating{{ $get->idcourse }} = 1;

            function ratingChecked{{ $get->idcourse }}(nilai){
                nilaiRating{{ $get->idcourse }} = parseInt(nilai);
                $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').addClass('disabled');
                for (let index = 0; index < 6; index++) {
                    $(`#ratingAlert-{{ $get->idcourse }} .fa-star-${index}`).removeClass('fa-star-checked')
                }
                $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').removeClass('disabled');
                for (let index = 1; index < nilai; index++) {
                    $(`#ratingAlert-{{ $get->idcourse }} .fa-star-${index}`).addClass('fa-star-checked')
                }
            }

            $('.fa-check').fadeOut();
            $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').click(function(){ 
                let url = (`{{url('/course/update-rating')}}?rating=${nilaiRating{{ $get->idcourse }}-1}&idcourse={{ $get->idcourse }}`);    
                $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').html(`Mengirim`);
                $.get(url, function(e){
                    if(e.status == true){
                        $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').html(`Kirim`);
                        $('.fa-check').fadeIn();
                        setTimeout(function(){
                            $('.fa-check').fadeOut();
                        }, 6000);
                        window.location.replace('/course/moodle/{{ $get->idcourse }}');
                    }else{
                        $('#ratingAlert-{{ $get->idcourse }} .btn-kirim-rating').html(`Kirim`);
                    }
                });
            });
        </script>
    @endforeach
@endpush