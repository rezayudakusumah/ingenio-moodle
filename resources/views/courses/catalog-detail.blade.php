@extends('layouts.ingenio-app')

@section('title', 'Detail Katalog')

@push('style')
    <style>
        body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4 {
            font-family: poppins;
            color: #000;
        }

        .btn.disabled {
            pointer-events: none;
        }

        .table-striped tbody tr:last-of-type {
            border-radius: 0px 0px 10px 10px;
        }

        .plyr {
            border-radius: 10px 10px 0 0;
        }

        .headline-wrap {
            margin-bottom: 2rem;
        }

        .headline-wrap.headline-center {
            text-align: center;
        }

        .headline-wrap h2 {
            font-family: poppins; 
            font-size: 50px; 
            font-weight: 800;
            color: #000; 
            margin-top: 0; 
            margin-bottom: 1rem; 
        }

        .headline-wrap h3 {
            font-family: poppins; 
            font-size: 24px; 
            font-weight: 800;
            color: #000; 
            margin-top: 0; 
            margin-bottom: 1rem; 
        }

        .headline-wrap p {
            font-family: poppins; 
            font-size: 20px; 
            font-weight: 400;
            color: #333; 
            margin-bottom: 0;
        }
        
        .catalog-detail-header {
            background: #f3f3f3; 
            color: #000;
        }

        .catalog-detail-header .row {
            display: flex;
            align-items: center;
            height: 250px;
        }

        .catalog-detail-header .content--wrap h1 {
            font-family: poppins; 
            font-size: 30px; 
            font-weight: 800; 
            margin-top: 0;
            margin-bottom: 0;
        }

        .catalog-detail-header .content--wrap p {
            font-family: poppins; 
            font-size: 14px; 
            font-weight: 500; 
            color: #999;
            margin-bottom: 0;
        }

        .catalog-detail-header .content--wrap p.content--breadcrumb {
            color: #000;
        }

        .catalog--topic-wrap,
        .catalog--goal-wrap,
        .catalog--description-wrap,
        .catalog--material-wrap {
            margin-bottom: 2rem;
        }

        .catalog--topic-wrap .headline-wrap,
        .catalog--goal-wrap .headline-wrap,
        .catalog--description-wrap .headline-wrap,
        .catalog--material-wrap .headline-wrap {
            text-align: left;
        }

        .catalog--topic-wrap .headline-wrap h2,
        .catalog--goal-wrap .headline-wrap h2,
        .catalog--description-wrap .headline-wrap h2,
        .catalog--material-wrap .headline-wrap h2 {
            font-size: 30px;
        }

        .catalog--description-wrap p {
            font-size: 14px;
        }

        .catalog--material-wrap .accordion>.card:not(:first-of-type) {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .catalog--material-wrap .accordion>.card:not(:last-of-type) {
            border-bottom: 1px solid #eee;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .catalog--material-wrap .accordion .card {
            border: 0;
            border-radius: 10px;
            box-shadow: 0px 5px 15px -5px rgba(0, 0, 0, 0.1);
        }

        .catalog--material-wrap .accordion .card .card-header {
            border: none;
            padding: 0;
        }

        .catalog--material-wrap .accordion .card .card-body {
            padding: 0;
        }

        .catalog--material-wrap .accordion .btn-link {
            background: #fff;
            font-weight: 600;
            color: #007bff;
            text-decoration: none;
            border-radius: 0;
            padding: 1rem 1.5rem;
            margin: 0;
        }

        .catalog--material-wrap .accordion .btn-link:focus,
        .catalog--material-wrap .accordion .btn-link:hover {
            color: #0056b3;
        }

        .catalog--material-wrap .table {
            margin-bottom: 0;
        }

        .catalog--material-wrap .table td {
            font-size: 14px;
            vertical-align: middle;
            border: none;
            padding: .75rem 1.5rem;
        }

        .catalog--sticky-wrap {
            position: sticky; 
            top: 100px; 
            margin-top: -275px; 
            margin-left: auto; 
            margin-right: 0;
        }

        .catalog--sticky-wrap .card {
            width: 90%; 
            border: none; 
            border-radius: 10px; 
            box-shadow: 0px 5px 15px -5px rgba(0, 0, 0, 0.1);
        }

        .catalog--sticky-wrap .card .card-button {
            display: block;
            width: 100%;
        }

        .catalog--sticky-wrap .card .card-button a {
            display: block;
            width: 100%;
            background: #4ca3d9;
            color: #fff;
            text-align: center;
            border-radius: 0px 0px 10px 10px;
            padding: 1rem 2rem;
            transition: all 0.3s;
        }

        .catalog--sticky-wrap .card .card-button a:hover {
            background: #039be0;
        }

        .catalog--sticky-wrap .card .card-button a.disabled {
            pointer-events: none;
            opacity: 0.7;
        }

        .catalog--sticky-wrap .card img {
            height: 177.19px; 
            border-radius: 10px 10px 0 0;
        }

        .catalog--sticky-wrap .card .rating {
            font-family: poppins; 
            font-size: 16px; 
            font-weight: 500;
            margin-bottom: 0;
        }

        .catalog--sticky-wrap .card .rating i {
            color: gold;
        }

        .catalog--sticky-mobile {
            display: none;
        }

        .card-rating,
        .modal-rating {
            text-align: center;
        }

        .card-rating .rating-headline,
        .modal-rating .rating-headline {
            font-size: 18px; 
            font-weight: 700; 
            margin-bottom: 0;
        }

        .card-rating .rating-text,
        .modal-rating .rating-text {
            font-size: 14px;
        }

        .card-rating .rating-wrap,
        .modal-rating .rating-wrap {
            font-size: 24px; 
            margin-top: 2rem;
        }

        @media(max-width: 420px) {
            .catalog--sticky-wrap {
                display: none;
            }

            .catalog--sticky-mobile {
                position: sticky;
                display: block;
                background: #fff;
                box-shadow: 0px -5px 15px -5px rgba(0, 0, 0, 0.1);
                padding: 2rem;
                bottom: 0;
            }

            .catalog--sticky-mobile .flex-wrap {
                display: flex;
                align-items: center;
                justify-content: space-evenly;
            }
        }

        .fa-star-checked {
            color: orange;
        }

        .fa-star {
            cursor: pointer;
        }
    </style>
@endpush

@section('content')
    <div class="catalog-detail-header">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="content--wrap">
                        @if(Auth::check())
                        <p class="content--breadcrumb"><a href="/my" style="color: #07c75e;">Home</a> / {{$course->fullname}}</p>
                        @else
                        <p class="content--breadcrumb"><a href="/" style="color: #07c75e;">Home</a> / {{$course->fullname}}</p>
                        @endif
                        <h1>{{$course->fullname}}</h1>                        
                        <p><i class="fas fa-star" style="color: gold;"></i>0.0 (0 Penilaian)</p>
						<p class="tutor">Oleh John Doe</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="margin-bottom: -2rem;">
					<div class="catalog--description-wrap">
                        <div class="headline-wrap">
                            <h2>Deskripsi</h2>
                        </div>
                        <p>{{$course->summary}}</p>
					</div>

                    <div class="catalog--topic-wrap">
                        <div class="headline-wrap">
                            <h2>Topik Pembelajaran</h2>
                        </div>
                        <p>{{$course->topik}}</p>
                    </div>

                    <div class="catalog--topic-wrap">
                        <div class="headline-wrap">
                            <h2>Tujuan Pembelajaran</h2>
                        </div>
                        <p>{{$course->tujuan}}</p>
                    </div>

                    <div class="catalog--material-wrap">
                        <div class="headline-wrap">
                            <h2>Materi kelas</h2>
                        </div>
                        <div class="accordion" id="materialAccordion">
                            <div class="card">
                                <div class="card-header">
                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse" style="color: #07c75e;">
                                        Sample Section
                                    </button>
                                </div>
                            
                                <div id="collapse" class="collapse show" data-parent="#materialAccordion">
                                    <div class="card-body">
                                        <table class="table table-striped">
                                            <tr>
                                                <td>
                                                    Sample Materi 1
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Sample Materi 2
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="catalog--sticky-wrap">
                        <div class="card" style="margin-bottom: 2rem;">
							
							<img src="/assets/img/dummy.jpg" alt="">

                            <div class="card-body">
                                <p style="display: flex; align-items: center; justify-content: space-between; font-size: 14px; font-weight: 400; line-height: 1.5; margin-bottom: 0;"><span style="font-weight: 700;">Durasi:</span>{{$course->durasi}} Jam</p>
                                <p style="display: flex; align-items: center; justify-content: space-between; font-size: 14px; font-weight: 400; line-height: 1.5; margin-bottom: 0;"><span style="font-weight: 700;">Level:</span>{{$course->level}}</p>
                                <p style="display: flex; align-items: center; justify-content: space-between; font-size: 14px; font-weight: 400; line-height: 1.5; margin-bottom: 0;"><span style="font-weight: 700;">Rekomendasi:</span>{{$course->rekomendasi}} Jam / Minggu</p>

                                <hr style="margin-top: 1rem; margin-bottom: 1rem;">

                                <label id="cbox" style="font-size: 14px; color: #333;">
                                    <input type="checkbox" name="" id=""> Saya telah membaca <a href="#" data-toggle="modal" data-target="#enrollPolicy" style="color: #07c75e;">kebijakan kelas</a>
                                </label>
                            </div>
                            <div class="card-button">
                                @guest
									<a class="disabled" href="" style="background-color: #07c75e;">Enroll Course</a>
								@else
									@php 
                                        $iduser = Auth::user()->idmoodle;
                                        $course_enroll = get_user_enrol($course->idcourse, $iduser);
                                    @endphp
									
									@if($course_enroll == 0)
                                        <a class="disabled" href="/enrol/{{$course->idcourse}}" style="background-color: #07c75e;">Enroll Course</a>      
									@else
                                        <a href="/course/moodle/{{$course->idcourse}}" style="background-color: #07c75e;">Akses Kelas</a> 
									@endif
										
								@endguest
                            </div>
                        </div>
                        @if(Auth::check())
                        {{-- @if(!empty($enrollment)) --}}
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-rating">
                                        <p class="rating-text">Penilaian Anda untuk kelas ini</p>
                                        <div class="rating-wrap" style="margin-top: 1rem; margin-bottom: 1rem;">
                                            <span class="fas fa-star fa-star-1"></span>
                                            <span class="fas fa-star fa-star-2"></span>
                                            <span class="fas fa-star fa-star-3"></span>
                                            <span class="fas fa-star fa-star-4"></span>
                                            <span class="fas fa-star fa-star-5"></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-button">
                                    <a href="#" data-toggle="modal" data-target="#ratingModal" style="background-color: #07c75e;">Beri penilaian</a>
                                </div>
                                
                            </div>
                        {{-- @endif --}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="catalog--sticky-mobile">
        <label style="font-size: 14px; color: #333;">
            <input type="checkbox" name="" id=""> Saya telah membaca <a href="#" data-toggle="modal" data-target="#enrollPolicy">kebijakan kelas</a>
        </label>
        <div class="flex-wrap">
            @guest
                <a class="btn btn-raised" href="#" data-toggle="modal" data-target="#enrollPolicy">Enroll Course</a>
            @else
                @php 
                    $iduser = Auth::user()->idmoodle;
                    $course_enroll = get_user_enrol($course->idcourse, $iduser);
                @endphp
            
                @if($course_enroll == 0)
                    <a class="disabled" href="/enrol/{{$course->idcourse}}">Enroll Course</a>      
                @else
                    <a href="/course/moodle/{{$course->idcourse}}">Akses Kelas</a> 
                @endif
            @endguest

            @if(Auth::check())
            <a class="btn btn-raised" href="#" data-toggle="modal" data-target="#ratingModal">Beri Penilaian</a>
            @endif
            
        </div>
    </div>

    <div class="modal fade" id="enrollPolicy">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="d-flex align-items-center justify-content-end mb-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">
                                <i class="zmdi zmdi-close"></i>
                            </span>
                        </button>
                    </div>
                    <p>Dengan ini saya menyatakan siap mengikuti pelatihan dengan sungguh-sungguh dan hingga tuntas.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ratingModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-rating">
                        <p class="rating-headline">Beri rating pada kelas ini</p>
                        <p class="rating-text">Penilaian Anda sungguh sangat bermakna bagi kami :)</p>
                        <div class="rating-wrap">
                            <span class="fas fa-star fa-star-1"></span>
                            <span class="fas fa-star fa-star-2"></span>
                            <span class="fas fa-star fa-star-3"></span>
                            <span class="fas fa-star fa-star-4"></span>
                            <span class="fas fa-star fa-star-5"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-raised btn-default no-shadow" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-raised btn-primary btn-kirim-rating disabled no-shadow">Kirim</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

{{-- penilaian bintang --}}
<script>
    $("#cbox").change(function() {
        $(".card-button a.disabled").toggleClass("disabled");
    });

    $('.fa-star-1').click(function(){ratingChecked(2)})
    $('.fa-star-2').click(function(){ratingChecked(3)})
    $('.fa-star-3').click(function(){ratingChecked(4)})
    $('.fa-star-4').click(function(){ratingChecked(5)})
    $('.fa-star-5').click(function(){ratingChecked(6)})
    let nilaiRating = 1;

    function ratingChecked(nilai){
      nilaiRating = parseInt(nilai);
      $('.btn-kirim-rating').addClass('disabled');
      for (let index = 0; index < 6; index++) {
        $(`.fa-star-${index}`).removeClass('fa-star-checked')
      }
      $('.btn-kirim-rating').removeClass('disabled');
      for (let index = 1; index < nilai; index++) {
        $(`.fa-star-${index}`).addClass('fa-star-checked')
      }
    }

    const idcourse = '';
    $('.fa-check').fadeOut();
    $('.btn-kirim-rating').click(function(){
      let url = (`{{url('/course/update-rating')}}?rating=${nilaiRating-1}&idcourse=${idcourse}`);
      $('.btn-kirim-rating').html(`Mengirim`);
      $.get(url, function(e){
        if(e.status == true){
          $('.btn-kirim-rating').html(`Kirim`);
          $('.fa-check').fadeIn();
          setTimeout(function(){
            $('.fa-check').fadeOut();
          }, 6000);
          location.reload();
        }else{
          $('.btn-kirim-rating').html(`Kirim`);
        }
      });
    });

    // console.log(`$lmsUpdateRating['rating']`);

    let dataRating = parseInt(``);
    for (let index = 1; index < dataRating+1; index++) {
    $(`.fa-star-${index}`).addClass('fa-star-checked')
    }
</script>

@endpush