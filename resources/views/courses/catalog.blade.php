@extends('layouts.ingenio-app')

@section('title', 'Open Class Catalog')

@push('style')
    <style>
        body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4 {
            font-family: poppins;
            color: #000;
        }

        .modal .modal-dialog .modal-content .modal-header {
            align-items: center; 
            padding: 2rem;
        }

        .modal .modal-dialog .modal-content .modal-header .modal-title,
        .modal .modal-dialog .modal-content .modal-header .close {
            padding: 0; 
            margin: 0;
        }

        .filter-hero {
            display: flex; 
            align-items: center; 
            justify-content: space-between; 
            border-bottom: 1px solid #eee; 
            padding-bottom: 1rem; 
            margin-bottom: 2rem;
        }

        .filter-wrap {
            position: sticky; 
            top: calc(50px + 2rem);
        }

        .card--link {
            display: flex; 
            color: #333; 
            margin-bottom: 2rem;
        }

        .card--link .card {
            display: inline-flex;
            flex-direction: row;
            width: 100%;
            background: #f9f9f9;
            transition: all 0.2s;
            border: 1px solid #eee;
            border-radius: 5px;
            box-shadow: none;
            margin-bottom: 0;
            transition: all 0.2s;
        }

        .card--link .card:hover {
            background: #f3f3f3;
        }

        .card--link .card img {
            width: 250px; 
            height: 141px;
            object-fit: cover;
            border-radius: 5px 0 0 5px;
        }

        .card--link .card .card-block {
            display: flex;
            width: calc(100% - 250px);
            position: relative;
            padding: 1rem 2rem;
        }

        .card--link .card .card-block .card-block--content {
            width: calc(100% - 100px); 
            padding-right: 2rem;
        }

        .card-block--content .title {
            font-family: poppins;
            font-size: 16px; 
            font-weight: 700; 
            color: #333; 
            line-height: 1.5;
            margin-top: 0; 
            margin-bottom: 0;
        }

        .card-block--content .tutor,
        .card-block--content .rating {
            font-family: poppins;
            font-size: 13px; 
            font-weight: 500; 
            line-height: 1.5; 
            color: #999;
            margin-bottom: 0;
        }

        .card-block--content .rating i {
            color: gold;
        }

        .card-block--price {
            width: 100px;
        }

        .card-block--price .price {
            font-weight: 700;
            text-align: right;
        }

        @media(max-width: 420px) {
            #filterDiv {
                display: none;
            }

            .card--link .card {
                flex-direction: column;
            }

            .card--link .card img {
                width: 100%;
                height: 150px;
                border-radius: 5px 5px 0 0;
            }

            .card--link .card .card-block {
                display: block;
                width: 100%;
            }

            .card--link .card .card-block .card-block--content {
                width: 100%;
                padding-right: 0;
            }

            .card-block--price {
                width: 100%;
            }
        }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    @if(Auth::check())
                    <li><a href="/my" style="color: #07c75e;">Home</a></li>
                    @else
                    <li><a href="/" style="color: #07c75e;">Home</a></li>
                    @endif
                    <li>Katalog</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="wrap bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- <div class="filter-hero">
                        {{-- FOR DESKTOP --}}
                        <button id="filterBtn" class="btn btn-raised btn-primary d-none d-sm-inline">
                            <i class="zmdi zmdi-sort-amount-desc"></i>Filter
                        </button>
                        {{-- FOR MOBILE --}}
                        <button class="btn btn-raised btn-primary d-sm-none d-inline" data-toggle="modal" data-target="#filterModal">
                            <i class="zmdi zmdi-sort-amount-desc"></i>Filter
                        </button>
                        <b>2 hasil</b>
                    </div> -->

                    <div class="row">
                        <!-- <div id="filterDiv" class="col-md-3">
                            <div class="filter-wrap">
                                <div class="card-category">
                                    <div class="card-block">
                                        <h3 class="headline-xs mt-0"><span>Tipe Kelas:</span></h3>
                                        <form action="#" method="GET">
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="gratis" value="1" {{(isset($gratis) ? 'checked' :'' )}} style="margin-right: .5rem;"> Gratis
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="berbayar" value="1" {{(isset($berbayar) ? 'checked' :'' )}} style="margin-right: .5rem;"> Berbayar
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="card-category">
                                    <div class="card-block">
                                        <h3 class="headline-xs mt-0"><span>Format:</span></h3>
                                        <form action="#" method="GET">
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="elearning" value="1" {{(isset($elearning) ? 'checked' :'' )}} style="margin-right: .5rem;"> E-Learning
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="inclass" value="2" {{(isset($inclass) ? 'checked' :'' )}} style="margin-right: .5rem;"> In Class
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="blended" value="3" {{(isset($blended) ? 'checked' :'' )}} style="margin-right: .5rem;"> Blended Learning
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="card-category">
                                    <div class="card-block">
                                        <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
                                        <form action="#" method="GET">
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="elearning" value="1" style="margin-right: .5rem;"> Semua Kategori
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="inclass" value="2" style="margin-right: .5rem;"> Teknologi
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="blended" value="3" style="margin-right: .5rem;"> Bisnis
                                            </div>
                                            <div class="form-group no-m no-p">
                                                <input type="checkbox" name="blended" value="3" style="margin-right: .5rem;"> Desain
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> -->

                        <div id="courseDiv" class="col-md-12">
                            @foreach($courses as $get)
                            <a class="card--link" href="/catalog/{{$get->idcourse}}">
                                <div class="card">
                                    <img src="/assets/img/dummy.jpg" alt="">
                                    <div class="card-block">
                                        <div class="card-block--content">
                                            <h3 class="title text-truncate">{{$get->fullname}}</h3>
                                            <p class="tutor">Oleh Ingenio</p>
                                            <p class="rating">
                                                <i class="fas fa-star"></i> 0.0 (0 Penilaian)
                                            </p>
                                        </div>
                                        <!-- <div class="card-block--price">
                                            <p class="price">Gratis</p>
                                        </div> -->
                                    </div>
                                </div>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="filterModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Filter</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div>
                    <div class="card-category">
                        <div class="card-block">
                            <h3 class="headline-xs mt-0"><span>Tipe Kelas:</span></h3>
                            <form action="#" method="GET">
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="gratis" value="1" {{(isset($gratis) ? 'checked' :'' )}} style="margin-right: .5rem;"> Gratis
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="berbayar" value="1" {{(isset($berbayar) ? 'checked' :'' )}} style="margin-right: .5rem;"> Berbayar
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card-category">
                        <div class="card-block">
                            <h3 class="headline-xs mt-0"><span>Format:</span></h3>
                            <form action="#" method="GET">
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="elearning" value="1" {{(isset($elearning) ? 'checked' :'' )}} style="margin-right: .5rem;"> E-Learning
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="inclass" value="2" {{(isset($inclass) ? 'checked' :'' )}} style="margin-right: .5rem;"> In Class
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="blended" value="3" {{(isset($blended) ? 'checked' :'' )}} style="margin-right: .5rem;"> Blended Learning
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card-category">
                        <div class="card-block">
                            <h3 class="headline-xs mt-0"><span>Kategori:</span></h3>
                            <form action="#" method="GET">
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="elearning" value="1" style="margin-right: .5rem;"> Semua Kategori
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="inclass" value="2" style="margin-right: .5rem;"> Teknologi
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="blended" value="3" style="margin-right: .5rem;"> Bisnis
                                </div>
                                <div class="form-group no-m no-p">
                                    <input type="checkbox" name="blended" value="3" style="margin-right: .5rem;"> Desain
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#filterBtn').click(function() {
            $('#filterDiv').toggleClass('d-none');
            $('#courseDiv').toggleClass('col-md-9');
            $('#courseDiv').toggleClass('col-md-12');
        })
    </script>
@endpush