@extends('layouts.ingenio-app')

@section('title', 'Masuk')

@push('style')
    <style>
        .checkbox input[type=checkbox]:checked+.checkbox-material:after {
            top: 0;
        }
    </style>
@endpush

@section('content')
    <div class="bg-page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><span>Masuk</span></h1>
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li>Masuk</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section>
        <div class="wrap pt-2 pb-2 mb-2 bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mx-auto">
                        {{-- <h1 style="font-size: 30px; font-weight: 700; color: #333; text-align: center; text-transform: uppercase;">Masuk</h1> --}}
                        <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group label-floating is-empty">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-account"></i>
                                        </span>
                                        <label class="control-label" for="ms-form-user">Email</label>
                                        <input name="email" type="email" value="{{old('email')}}" id="ms-form-user" class="form-control" required autofocus> 
                                    </div>
                                </div>
                                <div class="form-group label-floating is-empty">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="zmdi zmdi-lock"></i>
                                        </span>
                                        <label class="control-label" for="ms-form-pass">Password</label>
                                        <input type="password" name="password" id="ms-form-pass" class="form-control" required> 
                                    </div>
                                </div>
                                <div class="form-group mt-0">
                                    <div class="checkbox">
                                        <label style="display: inline-flex; align-items: center; margin: 0;">
                                            <input type="checkbox"> Remember 
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-block" style="background-color: #07c75e;">Masuk</button>
                                    <a class="btn btn-dark btn-block" href="{{ route('password.request') }}">Lupa password?</a>
                                </div>
                            </fieldset>
                        </form>
                        
                        {{-- <div class="card">
                            <div class="card-header">{{ __('Login') }}</div>

                            <div class="card-body">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    {{ __('Remember Me') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Login') }}
                                            </button>

                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
