<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\EnrolController;
use App\Http\Controllers\LMSController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\IndexController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('landingpage');
// });

Route::prefix('admin')->middleware(['auth','admin'])->group(function () {

    Route::get('/dashboard', [IndexController::class, 'index']);

    Route::resource('course', App\Http\Controllers\Admin\CourseController::class);

    Route::resource('category', App\Http\Controllers\Admin\CategoryController::class, ['only' => [
        'create', 'store', 'index'
    ]]);

    Route::resource('user', App\Http\Controllers\Admin\UserController::class);

});

Route::resource('catalog', CourseController::class, ['only' => [
    'index', 'show'
]]);

Route::get('/my', [CourseController::class, 'my'])->middleware('auth');
Route::get('/enrol/{id}', [EnrolController::class, 'enrol'])->middleware('auth');
Route::get('/', [LoginController::class, 'showLoginForm']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/course/moodle/{id}', [LMSController::class, 'course'])->middleware('auth');
Route::get('/moodle/admin', [LMSController::class, 'admin'])->middleware('auth','verified');
Auth::routes();
